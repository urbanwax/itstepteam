from flask import Flask, jsonify, request
from config import info

app = Flask(__name__)


@app.route("/", methods=['GET'])
def index():
    return jsonify(info)


@app.route("/api/app", methods=['GET', 'POST'])
def endpoint_app():
    pass


@app.route("/api/item", methods=['GET', 'POST'])
def endpoint_item():
    pass


if __name__ == '__main__':
    app.run(port=5001, debug=True)
