info = {
    'service_name': 'Generator',
    'version': 1.0,
    'language': 'Python',
    'Framework': 'Flask',
    'Database': 'Maria DB',
    'Author': 'ItStep Team',
    'URL': 'https://gitlab.com/urbanwax/itstepteam/-/tree/main/generator',
    'Documentation': 'https://gitlab.com/urbanwax/itstepteam/-/blob/main/generator/README.md'
}
