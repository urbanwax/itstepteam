# Documentation for Generator Service

## General Information

### Tooling
Generator will be a backend REST Api service written in Python.
Framework in use will be Flask.
### Purpose
Generator service will provide the means to handle mapping of existing applications to other services by generating unique Id for every item in every onboarded application.
All applications will have a number of items and each item will be given a unique id (UID). By this we will be able to map different items to other services for multiple applications.


## Api Endpoints
/api/app
 - POST will create(onboard) new applications
 - GET will return existing application and if not None
 - 
/api/item
 - POST will create a new item record and will return in.
 - GET will return item by app_id and item_id_in_app
 - GET will return item by uid

## Data structure
### Applications
- id
- name
### Items
- id
- uid
- app_id (FK from applications table)
- item_id_in_app

```mermaid
classDiagram
class Applications
Applications : +Int id
Applications : +String name

class Items
Items : +Int id
Items : +String uid
Items : +Int app_id
Items : +Int item_id_in_app
Items <|-- Applications : Items.app_id = Applications.id
```