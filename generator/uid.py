import hashlib


class UID():
    @classmethod
    def gen_uid(cls):
        source_string = 'sdfgerfhjbegvi'
        return hashlib.md5(source_string.encode()).hexdigest()


if __name__ == '__main__':
    print(UID.gen_uid())
