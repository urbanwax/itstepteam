# It Step team project

We will create a microservice web app that will serve crypto information to the public. It will also allow user to leave reviwes and to give a rating to a specific asset.

# Technologies involved
- Python
- Flask
- Web
- API

---

# Project will contain the following services
- Fullstack Flask based application for UI
- Rating Backend API service
- Reviews Backend API service
- UID Generator Backend Service
- Python job(console application) for common crypto assets info

## Fullstack Flask based application for UI
This will be a fullstack Flask based web app.
- HTML shoud be based on BOOTSTRAP.
- Main page will display all crypto assets ( 3 per line in a kind of box): The box will contain:
    - crypto asset name
    - Symbol in brackets after the name
    - asset Type and specificData/assetType below
    - market cap below
    - icon
    - read more link
- Read more link will open a page for the selected crypto asset and will provide all the information for the asset that we have together with the last 3 reviews (after the reviews we will have a read all reviews link)
- Read all reviews link will display tha sset name, rating and all the revies stored for this asset.


## Rating Backend API service
The service will use own dataset which will be a csv file so we can handle this using Pandas dataframes.
The service will provide options to:
- rate 1-5 stars per UID
- get rating for UID
- get all ratings
This service will be consumed by the `Fullstack Flask based application for UI`

## Reviews Backend API service
The service will use own database (preferably Mongo DB).
The service will provide options to:
- record a short review on a specific UID
- obtain last review for UID
- obtain last 3 (top 3) reviews for UID
- obtain all review per UID
This service will be consumed by the `Fullstack Flask based application for UI`


## UID Generator Backend Service
When we create a new crypto asset the Generator service will give us a UID (unique identifier) for each asset.
We need to obtain UID by assetName and vice versa.
Will use the same database as `Python job(console application) for common crypto assets info`

## Python job(console application) for common crypto assets info
Python job will be a python console app that will fill in the assets in the Database (MySQL). Job will consume data from https://cryptoapis.io/.
Job will be able to set the assets that we will need (First we will manualy create the crypto assets in the database). Setting initially an asset include the asset name whish corresponds to **assetName** field from the api. Each crypto asset will have a different UID obtained by the `UID Generator Backend Service`
Based on crypto asset list we will obtain information about them from https://cryptoapis.io/. This information will be stored in the database.
For every crypto asset we will need the follwing (obtained by https://developers.cryptoapis.io/technical-documentation/api/market-data/assets/get-asset-details-by-asset-id ):
- assetName
- assetOriginalSymbol
- assetType
- specificData/assetType
- marketCapInUSD
- assetLogo (we will need this to be stored in the database as path to the Logo so we can use this in the UI)
- latestRate

Use OOP in this python .

Use between 50 and 100 assets from the API

---
# Diagram

```mermaid
  graph TD;
      UI-->Rating;
      Rating-->css
      UI-->Reviews;
      Reviews-->MongoDB
      UI-->DB;
      Job-->DB;
      Generator-->DB;
      JOb-->Generator;
```